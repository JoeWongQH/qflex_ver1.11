﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deformCollider : MonoBehaviour {

	SkinnedMeshRenderer rend ;

	MeshCollider collide;

	Mesh  newMesh;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		newMesh = new Mesh();

	rend = GetComponent<SkinnedMeshRenderer>();
	collide = GetComponent<MeshCollider>();

		rend.BakeMesh (newMesh);
		collide.sharedMesh = newMesh;
			
	}
}
