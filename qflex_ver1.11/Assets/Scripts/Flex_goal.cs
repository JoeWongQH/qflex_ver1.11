﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace uFlex
{
	public class Flex_goal : FlexProcessor{ 


		public GameObject inObj;

		SkinnedMeshRenderer skRen;

		Mesh inMesh;

		public FlexParticles outpart;

		Vector3[] vertices;

		public bool goal = true;

	// Use this for initialization
	void Start () {
			
	}
	
	// Update is called once per frame
	void Update () {

			inMesh = new Mesh();
			skRen = inObj.GetComponent<SkinnedMeshRenderer> ();
			skRen.BakeMesh(inMesh);
			vertices = inMesh.vertices;




	}




		public override void PreContainerUpdate(FlexSolver solver, FlexContainer cntr, FlexParameters parameters)
		{


			for (int i = 0; i <outpart.m_maxParticlesCount; i++)
		{
				
						if (goal) {

					outpart.m_particles [i].pos = vertices [i];
					outpart.m_particles [i].invMass = 0.0f;

				}
			}
		
		}



}
}