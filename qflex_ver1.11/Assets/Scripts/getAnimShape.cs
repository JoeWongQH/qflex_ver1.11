﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getAnimShape : MonoBehaviour {

	public GameObject inSknShape;

	SkinnedMeshRenderer inSknRen;

	MeshCollider collider;

	Mesh snapShotMesh;

	MeshFilter currMesh;


	// Use this for initialization


	void Start () {
	
		inSknRen = inSknShape.GetComponent<SkinnedMeshRenderer> ();
		currMesh = GetComponent<MeshFilter> ();
		collider = GetComponent<MeshCollider> ();
		 
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		snapShotMesh = new Mesh ();
		inSknRen.BakeMesh (snapShotMesh);
		//print (snapShotMesh.normals [3]);
		currMesh.mesh = snapShotMesh; 
		collider.sharedMesh=snapShotMesh; 
	}
}
