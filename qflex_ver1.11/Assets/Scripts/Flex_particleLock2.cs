﻿using UnityEngine;
using System.Collections.Generic;

namespace uFlex
{
	/// <summary>
	/// Lock particles which are inside the collider
	/// WARNING. Tearable cloths will cause wrong behaviour
	/// </summary>
	public class Flex_particleLock2 : FlexProcessor
	{
		public bool m_lock = false;
		//public float m_markerSize = 0.3f;
		public bool m_followAnchorMovement = false;
		public float m_weightMultiplier = 0f;


		public List<int> m_lockedParticlesIds = new List<int>();
		public List<float> m_lockedParticlesMasses = new List<float>();

		public List<Transform> m_lockedMarkers = new List<Transform>();

		public override void FlexStart(FlexSolver solver, FlexContainer cntr, FlexParameters parameters)
		{
			for (int i = 0; i < cntr.m_particlesCount; i++)
			{
				Collider collider = GetComponent<Collider>();
				Collider[] colliders = Physics.OverlapSphere(cntr.m_particles[i].pos, 1.0f);
				foreach (Collider c in colliders)
				{
					if (c == collider)
					{
						m_lockedParticlesIds.Add(i);
						m_lockedParticlesMasses.Add(cntr.m_particles[i].invMass);
						cntr.m_particles[i].invMass = 0.0f;

						GameObject lockedParticle = new GameObject("lockedParticle_" + i);
						lockedParticle.transform.position = cntr.m_particles[i].pos;
						lockedParticle.transform.parent = this.transform;
						//JointMarker jm = lockedParticle.AddComponent<JointMarker>();
						//jm.markerSize = m_markerSize;
						m_lockedMarkers.Add(lockedParticle.transform);
					}
				}
			}
		}

		FlexContainer curr_cntr;
		public override void PostContainerUpdate(FlexSolver solver, FlexContainer cntr, FlexParameters parameters)
		{
			curr_cntr = cntr;
			for (int i = 0; i < m_lockedParticlesIds.Count; i++)
			{
				if (m_lock)
				{
					cntr.m_particles[m_lockedParticlesIds[i]].invMass = 0.0f;
					if(m_followAnchorMovement)
					{
						cntr.m_particles[m_lockedParticlesIds[i]].pos = m_lockedMarkers[i].position;
						cntr.m_particles[m_lockedParticlesIds[i]].invMass = m_lockedParticlesMasses[i]* m_weightMultiplier;
					}
				}
				else
				{
					cntr.m_particles[m_lockedParticlesIds[i]].invMass = m_lockedParticlesMasses[i];
				}
			}
		}    
	}
}