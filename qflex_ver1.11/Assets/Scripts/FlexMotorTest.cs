using UnityEngine;
using System.Collections;
using uFlex;

public enum CutAxis
{
	X,
	Y,
	Z
}

public class FlexMotorTest : FlexProcessor // MonoBehaviour
{
	private FlexShapeMatching m_flexShapeMatching;
	private FlexParticles m_particles;// = m_flexGameObjects[iId].GetComponent<FlexParticles>();
	public float scale = 1;
	public float rot = 0; // deg
	public float rotPos = 0;
	public Vector3 centerPos;
	public CutAxis cutAxis = CutAxis.Y;
	public float cutDistance = -3.25f;
	public float cutPercent;
	public float cutMovement;

	Vector3[] m_origShapeRestPositions;
	Vector3[] m_origParticlePositions;
	public Vector3[] m_origShapeCenters;

	void OnEnable()
	{
		if (m_flexShapeMatching == null)
			m_flexShapeMatching = GetComponent<FlexShapeMatching>();

		if (m_particles == null)
			m_particles = GetComponent<FlexParticles>();
	}

	public override void PreContainerUpdate(FlexSolver solver, FlexContainer cntr, FlexParameters parameters)
	{
		if (m_origShapeRestPositions == null || m_origShapeRestPositions.Length == 0)
		{
			m_origShapeCenters = new Vector3[m_flexShapeMatching.m_shapeCenters.Length];
			m_origParticlePositions = new Vector3[m_particles.m_particles.Length];
			m_origShapeRestPositions = new Vector3[m_flexShapeMatching.m_shapeRestPositions.Length];

			for (int i = 0; i < m_flexShapeMatching.m_shapeRestPositions.Length; i++)
			{
				m_origShapeRestPositions[i] = m_flexShapeMatching.m_shapeRestPositions[i];
			}

			for (int i = 0; i < m_flexShapeMatching.m_shapeCenters.Length; i++)
			{
				m_origShapeCenters[i] = m_flexShapeMatching.m_shapeCenters[i];
			}

			for (int i = 0; i < m_particles.m_particles.Length; i++)
			{
				int p = m_flexShapeMatching.m_shapeIndices[i];
				Vector3 pos = m_particles.m_particles[p].pos;
				m_origParticlePositions[i] = pos;
			}

		}

		Quaternion qRot = Quaternion.identity;
		switch (cutAxis)
		{
		case CutAxis.X: qRot = Quaternion.Euler(rot, 0, 0); break;
		case CutAxis.Y: qRot = Quaternion.Euler(0, rot, 0); break;
		case CutAxis.Z: qRot = Quaternion.Euler(0, 0, rot); break;
		}

		centerPos.Set(0, 0, 0);

		int xCutTotal = 0;
		FlexShapeMatching shapes = this.m_flexShapeMatching;

		int shapeIndex = 0;
		int shapeIndexOffset = shapes.m_shapesIndex;
		int shapeStart = 0;

		for (int s = 0; s < shapes.m_shapesCount; s++)
		{
			Vector3 shapeCenter = new Vector3();
			shapeIndex++;

			int shapeEnd = shapes.m_shapeOffsets[s];

			int shapeCount = shapeEnd - shapeStart;
			int origShapeIndexOffset = shapeIndexOffset;
			for (int i = shapeStart; i < shapeEnd; ++i)
			{
				Vector3 pos = m_origShapeRestPositions[i] + shapes.m_shapeCenters[s];

				float cutValue = 0;
				switch (cutAxis)
				{
				case CutAxis.X: cutValue = pos.x; break;
				case CutAxis.Y: cutValue = pos.y; break;
				case CutAxis.Z: cutValue = pos.z; break;
				}

				if (cutValue > cutDistance)
				{
					xCutTotal++;
					switch (cutAxis)
					{
					case CutAxis.X: pos.x += cutMovement; break;
					case CutAxis.Y: pos.y += cutMovement; break;
					case CutAxis.Z: pos.z += cutMovement; break;
					}

					pos = qRot * pos;

				}
				else
				{
					pos *= scale;
				}

				shapes.m_shapeRestPositions[shapeIndexOffset] = pos;
				shapeCenter += pos;
				shapeIndexOffset++;
			}

			shapeCenter /= shapeCount;

			// fix offsets, can't edit center position
			for (int i = shapeStart; i < shapeEnd; ++i)
			{
				Vector3 pos = shapes.m_shapeRestPositions[origShapeIndexOffset];
				pos -= shapeCenter;
				shapes.m_shapeRestPositions[origShapeIndexOffset] = pos;
				origShapeIndexOffset++;
			}

			shapeStart = shapeEnd;
		}
	}

}