﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class rayHit: MonoBehaviour
{
	public int sample = 9;
	Vector3[] randRay  ;
	public float radius = .1f;
	MeshFilter mF;
	Mesh mesh;
	Vector3[]  vert,normal;
	public GameObject inMesh;
	SkinnedMeshRenderer rend;
	Vector2 pixelUV;
	void Start()
	{


	rend = inMesh.GetComponent<SkinnedMeshRenderer>();
		rend.material.shader = Shader.Find ("Transparent/Diffuse");
		randRay = new Vector3[sample];
		for (int i = 0; i < sample; i++) {
			randRay [i] = Random.insideUnitSphere *radius;
		}
	
		mF = GetComponent<MeshFilter> ();
		mesh = mF.mesh;
		vert = mesh.vertices;
		normal = mesh.normals;
		for (int m = 0; m < vert.Length; m++) {
		
			vert [m] = transform.localToWorldMatrix.MultiplyPoint3x4 (vert [m]);

		}

		print (vert [5].ToString("F4"));
	}


	void Update()
	{
		Debug.DrawLine (vert [5], vert [5] - normal [5]*.1f);
		Texture2D tex = new Texture2D (512	, 512);

		RaycastHit[] hits;
		hits = new RaycastHit[vert.Length];
		for (int i =0 ; i < vert.Length ;i++){
			Physics.Raycast(vert[i],-normal[i],out hits[i], .2f);
		}

		for (int j = 0 ; j < sample ; j++)
		{
		for (int k = 0; k < hits.Length; k++)
		{
			 
			
				pixelUV = hits[k].textureCoord ;
				pixelUV.x *= tex.width+randRay [j].x*radius;
				pixelUV.y *= tex.height+randRay [j].y*radius;

		
				// Change the material of all hit colliders
				// to use a transparent shader.

				Color tempColor = rend.material.color;
				//tempColor.a = 0.3F;
				//rend.material.color = tempColor;
				
				tex.SetPixel((int)pixelUV.x, (int)pixelUV.y, Color.black);
				tex.Apply();
				rend.material.mainTexture = tex;
			

		}
		}
	}
}