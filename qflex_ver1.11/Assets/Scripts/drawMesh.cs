﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drawMesh : MonoBehaviour {

	// Use this for initialization
	public Mesh mesh;

	public void OnDrawGizmos() {
		// set first shader pass of the material

		// draw mesh at the origin
		Gizmos.DrawMesh(mesh, Vector3.zero, Quaternion.identity);
	}
}
