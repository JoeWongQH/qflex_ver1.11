﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace uFlex
{
	
	public class Flex_getParticle :  FlexProcessor  {

		public FlexParticles gBodyA;
		public FlexParticles gBodyB; 
		List<int> gBodyA_posId = new List<int>();
		List<int> gBodyB_posId = new List<int>();
		int gBodyACnt=0;
		int gBodyBCnt=0;
		public bool gLock = false;

		void pair ()
		{
			for (int i = 0; i <gBodyA.m_maxParticlesCount; i++) 
			{

				for (int j = 0; j < gBodyB.m_maxParticlesCount; j++)
				{

					if (gBodyA.m_particles [i].pos == gBodyB.m_particles [j].pos) 
					{

						gBodyA_posId.Add(i); 
						gBodyACnt++;
						gBodyB_posId.Add(j);
						gBodyBCnt++;
					}

				}

			}
		}
		
		void Start()
		{
			
			pair();
			//print (gBodyA_posId.Count + " " +gBodyB_posId.Count);
			//print (gBodyA_posId [3] + " " + gBodyB_posId [3]);
		
		}



		public override void FlexStart(FlexSolver solver, FlexContainer cntr, FlexParameters parameters)
		{
			//print (cntr + " "+ cntr.m_particlesCount);
		}

	
		public override void PreContainerUpdate(FlexSolver solver, FlexContainer cntr, FlexParameters parameters)
		{


			for (int i = 0; i < gBodyA_posId.Count; i++)
			{
				

				if (gLock) {
					
					//gBodyA.m_particles [gBodyA_posId [i]].pos = gBodyB.m_particles [gBodyB_posId [i]].pos;
					gBodyA.m_particles [gBodyA_posId [i]].pos = gBodyB.m_particles [gBodyB_posId [i]].pos;
					gBodyA.m_particles [gBodyA_posId [i]].invMass = 0.0f;
				}
			}

		}


	}
}