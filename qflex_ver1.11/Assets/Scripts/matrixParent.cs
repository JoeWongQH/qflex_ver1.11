using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using uFlex;

namespace uFlex
{

public class matrixParent : FlexProcessor {
	public GameObject[] parent;
		public bool active = false;
	MemberData kk;	
	Collider col;
	Mesh childMesh;
	public Vector3[] vert;
	Vector3 childWolrdPos;
	List<int> tmpIndex;
	List<List<int>> pinList;
	FlexParticles fP;
	Particle[] fPData;
	public bool onDraw=false;
	Vector3 newPos;
	 List<Vector3> tmpinPos;
	 List<List<Vector3>> pinPos;
	List<List<Vector3>> tmpVert;
	// Use this for initialization

/*		public void Start() {
		pinList = new List<List<int>>();
	
		pinPos = new List<List<Vector3>>  ();
		
		tmpVert = new List<List<Vector3>> ();
		//fP = GetComponent<FlexParticles> ();
		//fPData = fP.m_particles;
		
			kk = (MemberData)Resources.Load (this.gameObject.name);
		
			List<Vector3> intiPos=new List<Vector3>();
			intiPos = kk.pPos;
			for (int s=0 ; s<parent.Length;s++) {
				col = parent [s].GetComponent<Collider> ();
				pinPos.Add (new List<Vector3> ());
				tmpVert.Add (new List<Vector3> ());
				pinList.Add ( calBound ( col, intiPos));
				for (int d =0 ; d<pinList[s].Count; d++) {
					tmpVert[s].Add (new Vector3());
					pinPos[s].Add (kk.pPos[pinList[s][d]]);

				}
			


		//		print (tmpinPos.Count +" "+tmpIndex.Count);
			}
			print ("yy");


			for (int j = 0; j < pinPos.Count; j++) {
				
			
				for (int k = 0 ; k < pinPos[j].Count ; k++) {
					
					pinPos [j] [k] = transform.TransformPoint (pinPos [j] [k]);
					pinPos [j] [k] = parent [j].transform.InverseTransformPoint (pinPos [j] [k]);
					//print (pinPos [j] [k]);
				}

			}

	}
	*/
		public void getInstancePin()
		{
			
			pinList = new List<List<int>>();

			pinPos = new List<List<Vector3>>  ();

			tmpVert = new List<List<Vector3>> ();
			fP = GetComponent<FlexParticles> ();
			fPData = fP.m_particles;
			List<Vector3> intiPos=new List<Vector3>();
			foreach (Particle instPart in fPData) {
				intiPos.Add (instPart.pos);
			}
			for (int s=0 ; s<parent.Length;s++) {
				col = parent [s].GetComponent<Collider> ();
				pinPos.Add (new List<Vector3> ());
				tmpVert.Add (new List<Vector3> ());
				pinList.Add ( calBound ( col, intiPos));
				for (int d =0 ; d<pinList[s].Count; d++) {
					tmpVert[s].Add (new Vector3());
					pinPos[s].Add (fPData[pinList[s][d]].pos);

				}


			}
			for (int j = 0; j < pinPos.Count; j++) {


				for (int k = 0 ; k < pinPos[j].Count ; k++) {

					pinPos [j] [k] = transform.TransformPoint (pinPos [j] [k]);
					pinPos [j] [k] = parent [j].transform.InverseTransformPoint (pinPos [j] [k]);
					//print (pinPos [j] [k]);
				}

			}

		}
	public List<int> calBound( Collider inBound , List<Vector3> infP)
	{
		List<int> cbOutput = new List<int>() ;
			for (int i=0 ; i<infP.Count ; i++)
			{
			if (inBound.bounds.Contains(infP[i]))
				{
				cbOutput.Add(i);
				}
			}
			return cbOutput;

	} 
	// Update is called once per frame
	void Update () {
			//print (pinPos.Count);
			for (int l = 0; l <pinPos.Count; l++) {

				Matrix4x4 parentM = Matrix4x4.TRS (parent [l].transform.localPosition, parent [l].transform.localRotation, parent [l].transform.lossyScale);
				Matrix4x4 parentOrg = Matrix4x4.TRS (Vector3.zero, Quaternion.Euler (0, 0, 0), Vector3.one);


				for (int m = 0 ; m < pinPos[l].Count ; m++)
				{	
					
					newPos = parentOrg.MultiplyPoint3x4 (pinPos [l] [m]);
					newPos = parent[l].transform.TransformPoint (newPos);

					tmpVert[l][m] = transform.InverseTransformPoint (newPos);

				
				}

			}

		
	}


	public override void PreContainerUpdate(FlexSolver solver, FlexContainer cntr, FlexParameters parameters){

			if (active)
			{
			for (int n = 0; n < pinList.Count; n++)
				for (int o=0 ; o<pinList[n].Count;o++) {
				
					fPData [pinList [n] [o]].pos = 	tmpVert [n] [o];
				
				}
		}
		}
	void OnDrawGizmos()
	{
	
		Gizmos.color = Color.yellow;
		if (onDraw)
			Gizmos.DrawWireSphere (newPos , .003f);
	}

}
}