﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using uFlex;

public class ppCollider : MonoBehaviour {

	public GameObject ppInColider ;

	SkinnedMeshRenderer ppMeshRen;

	public int reduce=1;

	public float boundSize=1;

	public FlexParticles inputCloth;

	Mesh ppColliderMesh;

	GameObject[] allCol;

	Vector3[] ppNormals ;

	List<Vector3> pointInBound = new List<Vector3>();

	List<int> pId = new List<int> ();
	bool calBound(FlexParticles calInputCloth , Vector3 inPos)
	{
		bool inBoundTest =false;


		var transform = this.transform;
		Bounds inMeshBound = calInputCloth.m_bounds; 
		inMeshBound.center = inMeshBound.center+calInputCloth.transform.position;
		inMeshBound.size=inMeshBound.size*boundSize;
		if (inMeshBound.Contains (transform.TransformPoint(inPos)))
		{
			inBoundTest = true;
		}

		return inBoundTest;
	}
	// Use this for initialization
	void Awake() {

		ppMeshRen = GetComponent<SkinnedMeshRenderer>();
		ppColliderMesh = new Mesh ();
		ppMeshRen.BakeMesh(ppColliderMesh);
		GameObject ppColGrp = new GameObject ("collide_grp");

		for (int i = 0; i < ppColliderMesh.vertices.Length; i = i + reduce) {
			if (calBound (inputCloth, ppColliderMesh.vertices [i])) {
			
				pointInBound.Add (ppColliderMesh.vertices [i]);
				pId.Add (i);
			}
			
		}

		ppNormals = new Vector3[ppColliderMesh.normals.Length];

		allCol = new GameObject[ppColliderMesh.vertices.Length];
		//print (ppColliderMesh.normals.Length + " " + ppColliderMesh.vertices.Length);
		for (int j = 0; j <pointInBound.Count; j++) {
			allCol [j] = Instantiate (ppInColider);
			ppNormals[j] = ppColliderMesh.normals[pId[j]]*-1f;
			Quaternion ppRot = Quaternion.LookRotation (ppNormals [j]);
			allCol [j].transform.rotation = ppRot;
			allCol [j].transform.SetParent (ppColGrp.transform);		
		}
		
	}

	// Update is called once per frame
	void Update () {
		ppColliderMesh = new Mesh();
		ppMeshRen.BakeMesh(ppColliderMesh);
		//print (ppColliderMesh.vertices [10]);

		for (int k = 0; k < pointInBound.Count; k++) {
		
			allCol [k].transform.position=new Vector3(ppColliderMesh.vertices[pId[k]].x,ppColliderMesh.vertices[pId[k]].y,ppColliderMesh.vertices[pId[k]].z);


		
		}
	}
}
