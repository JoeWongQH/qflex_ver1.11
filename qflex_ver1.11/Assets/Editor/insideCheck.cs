﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using uFlex;


namespace uFlex{
public class insideCheck : MonoBehaviour {

	public FlexParticles fP ;
	public Collider[] col;
	Particle[] clothP;
	List<int> pinMem = new List<int>();
	List<int> pinCloth = new List<int>();
	List<int> pair = new List<int> ();
	public Transform root;
 	Mesh mesh;
	public bool onDraw = false;
	Vector3[] vert,fPvert;
	// Use this for initialization
	void Start () {

		SkinnedMeshRenderer skMesh = GetComponent<SkinnedMeshRenderer> ();

		Transform thisMatri =root.transform;
		Mesh bake = new Mesh ();
		skMesh.BakeMesh (bake);
		vert = bake.vertices;
		clothP = fP.m_particles;

		for (int m = 0; m < col.Length; m++) {

			for (int l = 0; l < clothP.Length; l++) {
				Vector3 worldPosC = clothP [l].pos;
				
					if (col [m].bounds.Contains (clothP [l].pos)) {
						pinCloth.Add (l);
					}
				}
			for (int n = 0; n < vert.Length;n++) {
					if (col [m].bounds.Contains (vert [n])) {
						pinMem.Add (n);
					}
				}
		}
		
	}
	 void OnDrawGizmos()
	{	

		SkinnedMeshRenderer skMesh = GetComponent<SkinnedMeshRenderer> ();
		Transform thisMatri =root.transform;
		Mesh bake = new Mesh ();
		skMesh.BakeMesh (bake);
		vert = bake.vertices;
			clothP = fP.m_particles;
		Gizmos.color = Color.yellow;
		for (int j = 0; j < col.Length; j++) {
			for (int k = 0; k < clothP.Length; k++) {
						Vector3 worldPosC = clothP [k].pos;

						if (col [j].bounds.Contains (clothP [k].pos)) {
							if (onDraw)
								Gizmos.DrawSphere (clothP[k].pos, .003f);
						}
					}
			for (int i = 0; i < vert.Length; i++) {
				if (col [j].bounds.Contains (vert [i])) {
					if (onDraw)
						Gizmos.DrawWireSphere (vert [i], .003f);
				}
			}
		}
		//Gizmos.DrawSphere (transform.position, 10f);

		

		


	}
	// Update is called once per frame
	void Update () {
		
	}
}
}