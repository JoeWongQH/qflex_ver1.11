﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using uFlex;

namespace uFlex
{

	public class QFlex : EditorWindow
	{	
		string newName = "Cloth";
		float mass = 1.0f;
		float stretchStiffness = 1.0f;
		float bendStiffness =1.0f;
		float tetherStiffness = 0.0f;
		float tetherGive = 0.0f;
		float weldThreshold = 0.0000f;

		float maxStrain = 2.0f;
		int maxSplits = 4;

		bool tearable = false;
		FlexInteractionType interactionType = FlexInteractionType.None;
		int group = -1;
		Flex.Phase phase = 0;
		Mesh inputMesh;
		Color color;
		bool softMesh = true;
		GameObject go;
		Editor gameObjectEditor;
		public GameObject dripQHS, colliderGO , skinObj , motionModel , colliderModel ;
		FlexParticles clothPart;
		ppCollider colObj;

		controlBS cBs;
		public SkinnedMeshRenderer bsQH;

		float bsTargetV = 100f;
		float timeDrip =1f;
		public FlexParticles FP;
		//public string filePath  = "Assets/Resources/MemberData.asset";
		Particle[] pData;
		Vector3[]  intiPos;
		public bool checkPlayEnd=false;
		public void CreateClothInti(FlexParticles inParticle , String inName)
		{
			MemberData gData= new MemberData();
			gData.pPos = new List<Vector3>();
			pData = inParticle.m_particles;
			Debug.Log (inParticle);
			for (int i = 0; i <pData.Length; i++) {

				gData.pPos.Add (pData[i].pos);

			}



			AssetDatabase.CreateAsset(gData, "Assets/Resources/" + inName + ".asset");
			AssetDatabase.SaveAssets ();
			AssetDatabase.Refresh ();


		}

		private  void GenerateFromMesh(GameObject go)
		{
			color = new Color (UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value, 1.0f);
			Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
			Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

			Vector3[] vertices = inputMesh.vertices;
			int vertexCount = inputMesh.vertexCount;

			int[] triangles = inputMesh.triangles;
			int triIndicesCount = triangles.Length;
			int trianglesCount = triIndicesCount / 3;

			IntPtr flexAssetPtr = IntPtr.Zero;
			int[] uniqueVerticesIds = new int[vertexCount];
			int[] origToUniqueVertMapping = new int[vertexCount];
			int uniqueVerticesCount = FlexExt.flexExtCreateWeldedMeshIndices(vertices, vertexCount,  uniqueVerticesIds,  origToUniqueVertMapping, weldThreshold);
			Vector3[] uniqueVertices = new Vector3[uniqueVerticesCount];
			Vector4[] uniqueVerticesWithInvMass = new Vector4[uniqueVerticesCount];
			for (int i = 0; i < uniqueVerticesCount; i++)
			{
				uniqueVertices[i] = vertices[uniqueVerticesIds[i]];

				uniqueVerticesWithInvMass[i] = vertices[uniqueVerticesIds[i]];
				uniqueVerticesWithInvMass[i].w = 1.0f / this.mass;

				min = FlexUtils.Min(min, uniqueVertices[i]);
				max = FlexUtils.Max(max, uniqueVertices[i]);
			}
			int[] uniqueTriangles = new int[trianglesCount*3];
			for (int i = 0; i < trianglesCount * 3; i++)
			{
				uniqueTriangles[i] = origToUniqueVertMapping[triangles[i]];
			}

			flexAssetPtr = FlexExt.flexExtCreateClothFromMesh(uniqueVerticesWithInvMass, uniqueVerticesCount, uniqueTriangles, trianglesCount, stretchStiffness, bendStiffness, tetherStiffness, tetherGive, 0);
			if (flexAssetPtr != IntPtr.Zero)
			{      
				//Flex Asset Marshalling
				FlexExt.FlexExtAsset flexAsset = (FlexExt.FlexExtAsset)Marshal.PtrToStructure(flexAssetPtr, typeof(FlexExt.FlexExtAsset));
				Vector4[] particles = FlexUtils.MarshallArrayOfStructures<Vector4>(flexAsset.mParticles, flexAsset.mNumParticles);

				int[] springIndices = FlexUtils.MarshallArrayOfStructures<int>(flexAsset.mSpringIndices, flexAsset.mNumSprings * 2);
				float[] springRestLengths = FlexUtils.MarshallArrayOfStructures<float>(flexAsset.mSpringRestLengths, flexAsset.mNumSprings);
				float[] springCoefficients = FlexUtils.MarshallArrayOfStructures<float>(flexAsset.mSpringCoefficients, flexAsset.mNumSprings);

				int[] shapeIndices = FlexUtils.MarshallArrayOfStructures<int>(flexAsset.mShapeIndices, flexAsset.mNumShapeIndices);
				int[] shapeOffsets = FlexUtils.MarshallArrayOfStructures<int>(flexAsset.mShapeOffsets, flexAsset.mNumShapes);
				Vector3[] shapeCenters = FlexUtils.MarshallArrayOfStructures<Vector3>(flexAsset.mShapeCenters, flexAsset.mNumShapes);
				float[] shapeCoefficients = FlexUtils.MarshallArrayOfStructures<float>(flexAsset.mShapeCoefficients, flexAsset.mNumShapes);


				if (flexAsset.mNumParticles > 0)
				{
					FlexParticles part = go.AddComponent<FlexParticles>();
					part.m_particlesCount = flexAsset.mNumParticles;
					part.m_maxParticlesCount = flexAsset.mMaxParticles;
					part.m_particles = new Particle[flexAsset.mMaxParticles];
					part.m_restParticles = new Particle[flexAsset.mMaxParticles];
					part.m_smoothedParticles = new Particle[flexAsset.mMaxParticles];
					part.m_colours = new Color[flexAsset.mMaxParticles];
					part.m_velocities = new Vector3[flexAsset.mMaxParticles];
					part.m_densities = new float[flexAsset.mMaxParticles];
					part.m_phases = new int[flexAsset.mMaxParticles];
					part.m_particlesActivity = new bool[flexAsset.mMaxParticles];


					part.m_colour = color;
					part.m_interactionType = interactionType;
					part.m_collisionGroup = group;
					part.m_bounds.SetMinMax(min, max);

					for (int i = 0; i < flexAsset.mNumParticles; i++)
					{
						part.m_particles[i].pos = particles[i];
						part.m_particles[i].invMass = particles[i].w;
						part.m_restParticles[i] = part.m_particles[i];
						part.m_smoothedParticles[i] = part.m_particles[i];
						part.m_colours[i] = color;
						part.m_particlesActivity[i] = true;


						part.m_phases[i] = (int)phase;



						//if (spacingRandomness != 0)
						//{
						//    part.m_particles[i].pos  += UnityEngine.Random.insideUnitSphere * spacingRandomness;
						//}
					}

				}


				if (flexAsset.mNumSprings > 0)
				{
					FlexSprings springs = go.AddComponent<FlexSprings>();
					springs.m_springsCount = flexAsset.mNumSprings;
					springs.m_springIndices = springIndices;
					springs.m_springRestLengths = springRestLengths;
					springs.m_springCoefficients = springCoefficients;


				}

				if(flexAsset.mNumTriangles > 0)
				{
					FlexTriangles tris = go.AddComponent<FlexTriangles>();
					tris.m_trianglesCount = trianglesCount;
					tris.m_triangleIndices = uniqueTriangles; 

				}


				FlexShapeMatching shapes = null;
				if (flexAsset.mNumShapes > 0)
				{
					shapes = go.AddComponent<FlexShapeMatching>();
					shapes.m_shapesCount = flexAsset.mNumShapes;
					shapes.m_shapeIndicesCount = flexAsset.mNumShapeIndices;
					shapes.m_shapeIndices = shapeIndices;
					shapes.m_shapeOffsets = shapeOffsets;
					shapes.m_shapeCenters = shapeCenters;
					shapes.m_shapeCoefficients = shapeCoefficients;
					shapes.m_shapeTranslations = new Vector3[flexAsset.mNumShapes];
					shapes.m_shapeRotations = new Quaternion[flexAsset.mNumShapes];
					shapes.m_shapeRestPositions = new Vector3[flexAsset.mNumShapeIndices];

					int shapeStart = 0;
					int shapeIndex = 0;
					int shapeIndexOffset = 0;
					for (int s = 0; s < shapes.m_shapesCount; s++)
					{
						shapes.m_shapeTranslations[s] = new Vector3();
						shapes.m_shapeRotations[s] = Quaternion.identity;

						//    int indexOffset = shapeIndexOffset;

						shapeIndex++;

						int shapeEnd = shapes.m_shapeOffsets[s];

						for (int i = shapeStart; i < shapeEnd; ++i)
						{
							int p = shapes.m_shapeIndices[i];

							// remap indices and create local space positions for each shape
							Vector3 pos = particles[p];
							shapes.m_shapeRestPositions[shapeIndexOffset] = pos - shapes.m_shapeCenters[s];

							//   m_shapeIndices[shapeIndexOffset] = shapes.m_shapeIndices[i] + particles.m_particlesIndex;

							shapeIndexOffset++;
						}

						shapeStart = shapeEnd;



					}
				}

				if (flexAsset.mInflatable)
				{
					FlexInflatable infla = go.AddComponent<FlexInflatable>();
					infla.m_pressure = flexAsset.mInflatablePressure;
					infla.m_stiffness = flexAsset.mInflatableStiffness;
					infla.m_restVolume = flexAsset.mInflatableVolume;

				}
				if (softMesh)
				{

					Renderer rend = null;
		

						if (tearable)
						{
							FlexTearableMesh tearableMesh = go.AddComponent<FlexTearableMesh>();
							tearableMesh.m_stretchKs = stretchStiffness;
							tearableMesh.m_bendKs = bendStiffness;
							tearableMesh.m_maxSplits = maxSplits;
							tearableMesh.m_maxStrain = maxStrain;
						}
						else
						{
							go.AddComponent<FlexClothMesh>();
						}

						MeshFilter meshFilter = go.AddComponent<MeshFilter>();
						meshFilter.sharedMesh = this.inputMesh;
						rend = go.AddComponent<MeshRenderer>();



					Material mat = new Material(Shader.Find("Diffuse"));
					mat.name = this.newName + "Mat";
					mat.color = color;
					rend.material = mat;

				}

			}

		}

		private GameObject  getQHm()
		{
			object[] QHmodel;
			GameObject dripQH,outModelQHm; 
			QHmodel =  Resources.LoadAll("QHuman",typeof(GameObject)) ;
			for (int i = 0; i < QHmodel.Length; i++) {
				Debug.Log (QHmodel [i]);
			}
			dripQH = QHmodel [0] as GameObject;
			outModelQHm =Instantiate (dripQH);
			return outModelQHm;
		}
		private GameObject  getQHmMotion()
		{
			object[] QHmMotion;
			GameObject getQHmMotion,outModelQHmM; 
			QHmMotion =  Resources.LoadAll("Motions",typeof(GameObject)) ;
			for (int i = 0; i < QHmMotion.Length; i++) {
				Debug.Log (QHmMotion[i]);
			}
			getQHmMotion = QHmMotion[0] as GameObject;
			outModelQHmM =Instantiate (getQHmMotion);
			return outModelQHmM;
		}

		//private GameObject getCollideP()
		//{
		//	GameObject colPrefab,colNode;
		//	colPrefab = Resources.Load("Collider/collider_grp") as GameObject;
		//	colNode = Instantiate (colPrefab);

	
		private GameObject getQHmCollider()
		{
			object[] QHmCollider;
			GameObject getQHmCollider,outCollQHmM; 
			QHmCollider=  Resources.LoadAll("Q_Collider",typeof(GameObject)) ;
			for (int i = 0; i <  QHmCollider.Length; i++) {
				Debug.Log ( QHmCollider[i]);
			}
			getQHmCollider=  QHmCollider[0] as GameObject;
			outCollQHmM =Instantiate (getQHmCollider);
			return outCollQHmM;
		
		}

		static UnityEditor.Animations.AnimatorController CreateController()
		{	

			object[] motionAll= Resources.LoadAll("Animation",typeof(Motion)) ;
			object[] motionQH= Resources.LoadAll("Motions",typeof(Motion)) ;
			for (int y = 0; y< motionQH.Length ;y++) {
			
				Debug.Log (motionQH [y]);
			}
			Debug.Log (motionAll[0]);
			UnityEditor.Animations.AnimatorController aControl  = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath ("Assets/Resources/Controller/QHuman.controller");
			Motion idleMotion = motionAll [0] as Motion ;
			Motion tarMotion = motionQH [0] as Motion;
			UnityEditor.Animations.AnimatorState idleClip = aControl.AddMotion (idleMotion);
			UnityEditor.Animations.AnimatorState tarClip = aControl.AddMotion (tarMotion );
			UnityEditor.Animations.AnimatorStateTransition idTran = idleClip.AddTransition (tarClip);
			idTran.hasExitTime = true;
			idTran.duration = 1f;
			idleClip.speed = 5;
			return aControl;

		}

		[MenuItem("Tools/QFlex")]

		public static void ShowWindows()
		{
			FlexSolver solver = FindObjectOfType<FlexSolver>();
			if(solver == null)
			{
				Debug.Log("No FlexSolver in scene. Creating a default one (Assets/uFlex/Prefabs/Flex.prefab)");
				GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/uFlex/Prefabs/Flex_Qset_2.prefab");
				GameObject flex = GameObject.Instantiate(prefab);

				flex.name = "Flex";
				solver = flex.GetComponent<FlexSolver> ();
			}



			EditorWindow.GetWindowWithRect<QFlex>(new Rect(0, 0, 400, 550), false, "QFlex");

		}
		void OnGUI()
		{
			
			inputMesh = EditorGUILayout.ObjectField (new GUIContent ("Input Mesh", "The input mesh, note that using large meshes may result in very long processing times or even running out of memory"), inputMesh, typeof(Mesh), true) as Mesh;
			if (GUILayout.Button ("Gen Cloth")) 
			{

				go = new GameObject();
				go.name = inputMesh.name;
				GenerateFromMesh(go);
				FlexParticlesRenderer partRend = go.AddComponent<FlexParticlesRenderer>();
				partRend.enabled = false;
				Selection.activeGameObject = go;
				FlexSprings cSpring = go.GetComponent<FlexSprings> ();
				cSpring.m_overrideStiffness = true;
				cSpring.m_newStiffness = 1f;
			
			}

			if (GUILayout.Button ("Get Model")) {
				

				dripQHS = getQHm();
				//colliderGO = getCollideP();
				bsQH = dripQHS.GetComponentInChildren<SkinnedMeshRenderer>();
				skinObj =bsQH.gameObject;

				bsQH.SetBlendShapeWeight(0,100f);
				skinObj.AddComponent<deformCollider> ();
				skinObj.AddComponent<MeshCollider> ();
				MeshFilter tmpMFilter = skinObj.AddComponent<MeshFilter> ();
				tmpMFilter.sharedMesh = bsQH.sharedMesh;

				FlexSolver solver = FindObjectOfType<FlexSolver>();
				solver.deform_collider = true;
				//skinObj.AddComponent (typeof(ppCollider));
				//colObj = skinObj.GetComponent<ppCollider> ();

				//colObj.ppInColider = colliderGO;
				//colObj.reduce = 2;

				//clothPart = go.GetComponent<FlexParticles> ();
			
				//colObj.inputCloth =clothPart;
	

			

			}
			if (GUILayout.Button ("Drip")) {
					

				EditorApplication.isPlaying = true;

			}

			if (GUILayout.Button ("Create_Anim")) {

				Renderer MS;
				Material Mat,newMat;
				MS = go.GetComponent<Renderer> ();
				Mat = MS.sharedMaterial;
				 newMat = Resources.Load("Material/qFlex_cloth", typeof(Material)) as Material;
				Debug.Log (Mat+" "+ newMat );
				MS.sharedMaterial = newMat;

				motionModel = getQHmMotion();
				colliderModel = getQHmCollider();
				FlexSprings cSpring = go.GetComponent<FlexSprings> ();
				FlexSolver solver = FindObjectOfType<FlexSolver>();
				solver.deform_collider = false;
				cSpring.m_newStiffness = 2f;
				clothPart = go.GetComponent<FlexParticles> ();
				clothPart.m_overrideMass = true;
				clothPart.m_mass = 0.1f;
				MeshFilter[]	mm = colliderModel.GetComponentsInChildren<MeshFilter> ();
				GameObject[] allColObj =new GameObject[mm.Length];
				for (int i = 0; i < mm.Length; i++) {
					
					allColObj [i] = mm [i].gameObject;
					allColObj [i].AddComponent (typeof(MeshCollider));
					allColObj [i].GetComponent<MeshRenderer>().enabled = false;
					Debug.Log (allColObj[i]);
				}
				UnityEditor.Animations.AnimatorController motionCtl = CreateController();
			
				Animator aA = motionModel.GetComponent<Animator>();
				Animator bB = colliderModel.GetComponent<Animator> ();
				bB.runtimeAnimatorController = motionCtl;
				aA.runtimeAnimatorController = motionCtl;

			}

			GUIStyle bgColor = new GUIStyle();
			bgColor.normal.background = EditorGUIUtility.whiteTexture;

			if 	(inputMesh != null) {



				gameObjectEditor = Editor.CreateEditor (inputMesh);
				gameObjectEditor.OnInteractivePreviewGUI (GUILayoutUtility.GetRect (400, 400), EditorStyles.objectFieldThumb);

			}

		}
		void Update()
		{
			if (!EditorApplication.isPlaying && checkPlayEnd) {
				DestroyImmediate (dripQHS);
				DestroyImmediate (colliderGO);
				checkPlayEnd = false;
				go.AddComponent (typeof(loadInti));

				
			}
			if (EditorApplication.isPlaying && dripQHS.gameObject !=null) {
				float tmp = bsTargetV * ((timeDrip - Time.deltaTime) * 1f); 
				timeDrip = timeDrip - Time.deltaTime;
				bsQH.SetBlendShapeWeight(0, tmp);
				Debug.Log (timeDrip);
				if (timeDrip <= -.1) {
					FP = go.GetComponent<FlexParticles> ();
					CreateClothInti (FP , go.name);
					timeDrip=1f;
					checkPlayEnd = true;
					UnityEditor.EditorApplication.isPlaying = false;


				}
			} 
		}
	}
}