﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class meshDataPrint :EditorWindow {


	[MenuItem("Tools/meshDataPrint")]

	public static void ShowWindows()
	{
		EditorWindow.GetWindowWithRect<meshDataPrint>(new Rect(0, 0, 400, 550), false, "meshDataPrint");
	
	}


	void OnGUI()
	{

		if (GUILayout.Button ("Get")) {
			GameObject[] selObj;

			selObj = Selection.gameObjects;
			Mesh mesh = selObj [0].GetComponent<MeshFilter> ().sharedMesh;
		
			Debug.Log ("Vert : "+ mesh.vertices.Length);
			Debug.Log ("UV : "+ mesh.uv.Length);
			Debug.Log ("Tri : " + mesh.triangles.Length);
			//Debug.Log ("Sub : " + mesh.subMeshCount);
		}
	}
	}
