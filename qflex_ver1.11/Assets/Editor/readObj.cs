﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;



public class readObj : MonoBehaviour {
	loadXml xmlFile;
	int[][] childPos,mumPos; 
	public TextAsset objFile;
	public float spSize = .01f;
	List<Vector3> vertPos = new List<Vector3>();
	float v1,v2,v3;

	List<int> allPosId=new List<int>();

	GameObject sphere;
	void Start()
	{
		xmlFile = GetComponentInChildren<loadXml>();
		childPos=xmlFile.childNum;
		mumPos = xmlFile.mumNum;



		for (int i = 0; i < childPos.Length; i++) {
			for (int j = 0; j < childPos [i].Length; j++) {
				
				allPosId.Add (childPos [i] [j]);
				allPosId.Add (mumPos [i] [j]);
			}
		
		}
		allPosId.Distinct ();
		//allPosId.Sort ();

		//for (int k = 0; k < allPosId.Count; k++) {
		
			//print (allPosId [k]);
		
		//}
		sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		sphere.transform.localScale = new Vector3 (spSize ,spSize , spSize );
		string txt = objFile.text;
		string[] txtLine = txt.Split('\n');
		foreach (string tmpLine in txtLine )
		{
			string[] hold = tmpLine.Split (' ');
			if (hold [0] == "v") {
				float.TryParse (hold [1], out v1);
				float.TryParse (hold [2], out v2);
				float.TryParse (hold [3], out v3);
				vertPos.Add (new Vector3 (-v1, v2, v3));
			
			}


		}
		for (int i = 0; i < allPosId.Count; i++) {
			GameObject tmpObj =	Instantiate (sphere);
			tmpObj.name = "sphere" +( allPosId [i]-1);
			tmpObj.transform.position = vertPos [allPosId[i]-1]*.001f;
		
		}
	}
}
