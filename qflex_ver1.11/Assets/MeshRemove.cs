﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshRemove : MonoBehaviour {

	public Camera Cam1;
	Vector3 rayv3;
	// Use this for initialization
	void Start () {
		//Debug.Log (GetComponent<MeshFilter> ().mesh.vertices.Length);

		//Debug.Log (GetComponent<MeshFilter> ().mesh.triangles.Length);
		rayv3 = new Vector3 (0, 0, 0);
		Debug.Log(GetComponent<SkinnedMeshRenderer> ().sharedMesh.vertices.Length);
		Debug.Log(GetComponent<SkinnedMeshRenderer> ().sharedMesh.triangles.Length);
	}
	
	// Update is called once per frame
	void Update () {
		 

		if (Input.GetMouseButtonDown (0)) {
			//Debug.Log(GetComponent<MeshFilter>().mesh.vertices[GetComponent<MeshFilter>().mesh.triangles[0]]);

			//Debug.Log (GetComponent<MeshFilter> ().mesh.vertices.Length);
			//Debug.Log (GetComponent<MeshFilter> ().mesh.triangles.Length);
			Debug.Log("clicked!");
			rayv3 = Cam1.ScreenToWorldPoint (Input.mousePosition);
			Debug.DrawRay (Cam1.ScreenToWorldPoint (Input.mousePosition), Vector3.forward* 10, Color.green);
			RaycastHit hit;
			Ray ray =Cam1.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast (ray, out hit, 10000f)) {
				Debug.Log ("hit!");
				deleteTri (hit.triangleIndex);

			}
		}


			Debug.DrawRay (rayv3, Vector3.forward* 10, Color.green);

		
	}

	void deleteTri(int index){
		Destroy(GetComponent<MeshCollider>());
		Mesh mesh = GetComponent<SkinnedMeshRenderer> ().sharedMesh;
		int[] oldTriangles = mesh.triangles;
		int[] newTriangles = new int[mesh.triangles.Length - 3];

		int i = 0;
		int j = 0;
		while (j < mesh.triangles.Length) {
			if (j != index * 3) {
				newTriangles [i++] = oldTriangles [j++];
				newTriangles [i++] = oldTriangles [j++];
				newTriangles [i++] = oldTriangles [j++];

			} else {
				j += 3000;
			}
		}
		GetComponent<SkinnedMeshRenderer> ().sharedMesh.triangles = newTriangles;
		gameObject.AddComponent<MeshCollider> ();

	}

}
