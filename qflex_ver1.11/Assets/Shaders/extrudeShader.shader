﻿Shader "Example/Normal Extrusion" {
    Properties {
      _MainTint("Diffuse Tint" , Color) = (1,1,1,1)
      _MainTex ("Texture", 2D) = "white" {}
      _Amount ("Extrusion Amount", Range(-1,1)) = 0.001
    }





    SubShader {

      Tags { "RenderType" = "Opaque" }
     Cull Off
       CGPROGRAM
      #pragma surface surf Lambert vertex:vert
      struct Input {
          float2 uv_MainTex;

      };

    
      float4 _MainTint;
      float _Amount;
      void vert (inout appdata_full v) {
          v.vertex.xyz += v.normal * _Amount;
        
      }
      sampler2D _MainTex;
      void surf (Input IN, inout SurfaceOutput o) {
       
          o.Albedo =tex2D(_MainTex, IN.uv_MainTex).rgb*.5+_MainTint.rgb*.5;
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }