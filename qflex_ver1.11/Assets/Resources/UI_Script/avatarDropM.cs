using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using uFlex;
using ProgressBar;

[System.Serializable]
public class clothSet
{

	public GameObject[] allSet; 


}
public class avatarDropM : MonoBehaviour {

	public Dropdown drop;
	public GameObject[] menuItemIdle,menuItemCol,clothSet,currentTCloth;
	GameObject[] jName ;
	public RuntimeAnimatorController[]  menuItemAnim;
	public RuntimeAnimatorController  idleAnim;
	public Texture2D getTex;
	public GameObject currentItemIdle=null;
	public GameObject currentItemCol=null;
	public GameObject currentDisplay=null;
	public GameObject wasteCol = null;
	RuntimeAnimatorController Anim;
	public Animator animatorCol,animatorDispaly;
	public GameObject clothSet2;
	public  bool[] gender; 
	public GameObject[] clothUIGen;

	GameObject	skinObj,colObj;
	bool startDrip = false;
	public bool tran = true;
	public blendJoint bj;
	public Material tranMat;
	Material tmpMat;
	public float timer ;
	float blendW=100;
	float newtime;
	public float recordTime;
	public string aStateName;
	public FlexSolver FS;
	public bool onDressed =false;
	deformCollider dC;
	MeshCollider mC;
	SkinnedMeshRenderer dripMeshRen;
	public GameObject currentCloth;
	public GameObject progressBar,progressPer;
	ProgressRadialBehaviour progessRB;
	public int size = 0;
	public string[] colAddJointName,upColJN,loColJN,ouColJN;
	public clothSize cSize;
	public CreateQFlex CQF;
	public itemSelect upITS,loITS,ouITS,asITS;

	void Start()
	{
		
	}
	public void valueChange()
	{
		//foreach (GameObject cs in clothSet) {
		//	cs.SetActive (false);
		//}


		if (drop.value != 0) {
			if (currentItemIdle != null) {
				Destroy (currentItemIdle);
				currentItemIdle = Instantiate (menuItemIdle [drop.value - 1]);

				SetLayerRecursively (currentItemIdle, 8);
			} else {
				currentItemIdle = Instantiate (menuItemIdle [drop.value - 1]);

				SetLayerRecursively (currentItemIdle, 8);
			}
			if (currentItemCol != null) {
				Destroy (currentItemCol);

				currentItemCol = Instantiate (menuItemCol [drop.value - 1]);

				SkinnedMeshRenderer sK = currentItemCol.GetComponentInChildren<SkinnedMeshRenderer> ();
				tmpMat = sK.sharedMaterial;
				sK.material = tranMat;
			
				animatorCol=currentItemCol .GetComponent<Animator> ();
				animatorCol.enabled=false;
				animatorCol.runtimeAnimatorController =menuItemAnim[drop.value - 1];
			

				animatorCol.updateMode = AnimatorUpdateMode.AnimatePhysics;
			
				setColVis (currentItemCol);
				print (currentItemCol + " " + currentItemIdle);
				setBlendjoint (currentItemCol, currentItemIdle);



			} else {
				currentItemCol = Instantiate (menuItemCol [drop.value-1]);

				SkinnedMeshRenderer sK = currentItemCol.GetComponentInChildren<SkinnedMeshRenderer> ();
				tmpMat = sK.sharedMaterial;
				sK.material = tranMat;
				animatorCol=currentItemCol .GetComponent<Animator> ();
				animatorCol.enabled=false;
				animatorCol.runtimeAnimatorController =menuItemAnim[drop.value - 1];
			
				animatorCol.updateMode = AnimatorUpdateMode.AnimatePhysics;

				setColVis (currentItemCol);
				print (currentItemCol + " " + currentItemIdle);
				setBlendjoint (currentItemCol, currentItemIdle);

			}
			if (currentDisplay != null) {
				Destroy (currentDisplay);
				currentDisplay = Instantiate (menuItemIdle [drop.value - 1]);
				animatorDispaly=currentDisplay .AddComponent<Animator> ();
				animatorDispaly.runtimeAnimatorController=idleAnim;
				animatorDispaly.avatar= animatorCol.avatar;
			} else {
				currentDisplay= Instantiate (menuItemIdle [drop.value - 1]);
				animatorDispaly=currentDisplay .AddComponent<Animator> ();
				animatorDispaly.runtimeAnimatorController=idleAnim;
				animatorDispaly.avatar= animatorCol.avatar;

			}
			//clothSet [drop.value - 1].SetActive (true);
		} else {
			Destroy (currentItemIdle);
			Destroy (currentItemCol);
		
		}
		if (gender [drop.value - 1]) {
			
			itemSelect[] dreComp;
			dreComp = clothUIGen [0].GetComponentsInChildren<itemSelect> ();

		} else {
		

			itemSelect[] dreComp;
			dreComp = clothUIGen [1].GetComponentsInChildren<itemSelect> () ;

		}

	}


	void setCol (GameObject inColObj)
	{

		MeshFilter[]	mm =  inColObj.GetComponentsInChildren<MeshFilter> ();
		GameObject[] allColObj =new GameObject[mm.Length];

		for (int i = 0; i < mm.Length; i++) {

			allColObj [i] = mm [i].gameObject;
			MeshCollider tmpMC = allColObj [i].AddComponent (typeof(MeshCollider)) as MeshCollider;


			allColObj [i].GetComponent<MeshRenderer>().enabled = false;
			//Debug.Log (allColObj[i]);
		}

	}

	public void boxParent()
	{
		getChildWithName gJoint = new getChildWithName ();
		jName = gJoint.getChildWName (currentItemCol, colAddJointName);
		foreach (GameObject tmpObj in jName)
		{
			BoxCollider BC = tmpObj.AddComponent<BoxCollider> ();
			BC.size = new Vector3 (.05f, .05f, .05f);
			if (tmpObj.name == "Scapula_L") {
				BC.center = new Vector3 (.05f, -0.04f, -0.02f);
			}
			if (tmpObj.name == "Scapula_R") {
				BC.center = new Vector3 (-0.05f, 0.04f, 0.02f);
			}
			if (tmpObj.name == "Knee_L") {
				BC.center = new Vector3 (0.1f, 0.0f, 0.0f);
				BC.size = new Vector3 (.01f, .08f, .08f);
			}
			if (tmpObj.name == "Knee_R") {
				BC.center = new Vector3 (-0.1f, 0.0f, 0.0f);
				BC.size = new Vector3 (.01f, .08f, .08f);
			}
			if (tmpObj.name == "BackA_M") {
					BC.size = new Vector3 (.07f, .18f, .18f);
					BC.center = new Vector3 (-0.03f, 0.0f, 0.0f);

			}
			if (tmpObj.name == "Elbow_R") {
				BC.center = new Vector3 (-0.04f, 0.0f, 0.0f);

			}
			if (tmpObj.name == "Elbow_L") {
				BC.center = new Vector3 (0.04f, 0.0f, 0.0f);

			}

			if (tmpObj.name == "Ankle_L") {
			//	BC.center = new Vector3 (0.03f, 0.0f, 0.0f);

			}
			if (tmpObj.name == "Ankle_R") {
			//	BC.center = new Vector3 (-0.03f, 0.0f, 0.0f);

			}
		}

	}
	void setColVis(GameObject inColObjVis)
	{
		MeshFilter[]	mm =  inColObjVis.GetComponentsInChildren<MeshFilter> ();
		GameObject[] allColObj =new GameObject[mm.Length];

		for (int i = 0; i < mm.Length; i++) {

			allColObj [i] = mm [i].gameObject;
		

			allColObj [i].GetComponent<MeshRenderer>().enabled = false;

		}
	}
	/*
	void setPinBox(GameObject[] inJoint)
	{
		foreach (GameObject tmpObj in inJoint)
		{
			BoxCollider BC = tmpObj.AddComponent<BoxCollider> ();
			BC.size = new Vector3 (.05f, .05f, .05f);
			if (tmpObj.name == "Scapula_L") {
				BC.center = new Vector3 (.05f, -0.04f, -0.02f);
			}
			if (tmpObj.name == "Scapula_R") {
				BC.center = new Vector3 (-0.05f, 0.04f, 0.02f);
			}
		}
	
	
	}
	*/
public	void SetLayerRecursively(GameObject obj, int newLayer)
	{
		if (null == obj)
		{
			return;
		}

		obj.layer = newLayer;

		foreach (Transform child in obj.transform)
		{
			if (null == child)
			{
				continue;
			}
			SetLayerRecursively(child.gameObject, newLayer);
		}
	}

	public void setAnim(RuntimeAnimatorController inAnim)
	{
		Anim = inAnim;
	}

	public void setBlendjoint(GameObject inCol , GameObject inIdle)
	{
		
		bj=inIdle.AddComponent<blendJoint> ();
		bj.targetJoint = inCol;
	}
	public void drip()
	{


		dripMeshRen =currentItemIdle.GetComponentInChildren<SkinnedMeshRenderer> ();
		GameObject colSkinObj  =dripMeshRen.gameObject;
		dripMeshRen.SetBlendShapeWeight (size,100);
		dC =colSkinObj.AddComponent<deformCollider>();
		mC=colSkinObj.AddComponent<MeshCollider>();
		MeshFilter tmpMFilter = colSkinObj .AddComponent<MeshFilter> ();


		Animator aC = currentItemCol.GetComponent<Animator> ();
		aC.enabled = true;
			//clothSet [drop.value - 1].SetActive (false);

	}
	public void kickOn()
	{	
		FlexSolver solver = FindObjectOfType<FlexSolver>();

		FlexContainer cont = FindObjectOfType<FlexContainer> ();
		solver.deform_collider = true;
		if (onDressed) {
			
			//solver.m_colliders.ProcessColliders(solver.m_solverPtr,Flex.Memory.eFlexMemoryHost);
			cont.InitParticleArrays();

			solver.Start ();

		} 

		solver.enabled = true;

		startDrip = true;

		bj.intiBJ();
		bj.startBJ = true;
		progessRB = progressPer.GetComponent<ProgressRadialBehaviour> ();
		newtime = timer;
		progressBar.SetActive (true);
	}
	private void kickOff()
	{	

		getChildWithName gJoint = new getChildWithName ();

		FlexSolver solver = FindObjectOfType<FlexSolver>();
		boxParent ();

		bj.enabled = false;

		FlexSprings fSp = FindObjectOfType<	FlexSprings> ();
		fSp.m_newStiffness = 1f;
		GameObject[] upJoint = gJoint.getChildWName (currentItemCol, upITS.finJoint);
		if (CQF.upM != null) {
			CQF.upM.parent = upJoint;
			CQF.upM.getInstancePin ();
			CQF.upM.enabled = true;
			CQF.upM.active = true;
		}

		GameObject[] loJoint = gJoint.getChildWName (currentItemCol,loITS.finJoint);
		if (CQF.loM != null) {
			CQF.loM.parent = loJoint;
			CQF.loM.getInstancePin ();
			CQF.loM.enabled = true;
			CQF.loM.active = true;

		}


	
		progressBar.SetActive (false);
		solver.enabled = false;
		solver.deform_collider = false;
		dC.enabled = false;
		mC.enabled = false;
		currentItemIdle.SetActive(false);
	
		MeshCollider[] allMC =FindObjectsOfType<MeshCollider>();
		foreach (MeshCollider MC in allMC) {
			MC.enabled = true;
		}
		setCol (currentItemCol);
		solver.m_colliders.ProcessColliders(solver.m_solverPtr,Flex.Memory.eFlexMemoryHost);

			StartCoroutine (changeDisp ());

		solver.enabled = true;




		onDressed = true;
		Animator currentAnim = currentItemCol.GetComponent<Animator> ();
		currentAnim.enabled = true;


		cSize.currentClothBut = 5;
		}

		


	IEnumerator changeDisp()
	{
		
		Animator aT = currentItemCol.GetComponent<Animator> ();
		AnimatorStateInfo aF = aT.GetCurrentAnimatorStateInfo (0);
		AnimatorClipInfo[] aC = aT.GetCurrentAnimatorClipInfo (0);
		print (aF.normalizedTime + " normailizedTime" );
		FlexClothMesh[] allCloth = FindObjectsOfType<FlexClothMesh>();

		if (onDressed) {
		
			for (float j = 0; j < 5f; j += Time.deltaTime) {
				aF = aT.GetCurrentAnimatorStateInfo (0);
				 aC = aT.GetCurrentAnimatorClipInfo (0);

				if ((aC [0].clip.name == aStateName) && (aF.normalizedTime>=recordTime)){
					print ("check");

				} else {
				
					yield return 0;
				}
			}
		} else {
		 
			for (float i = 0; i < 1.5f; i += Time.deltaTime) {
			


				yield return 0;
			}

		}
		Destroy (currentDisplay);
		Destroy (currentItemIdle);
	
		if (onDressed) {

			Destroy (wasteCol);
			SetLayerRecursively (currentItemCol, 1);
		}
		foreach (FlexClothMesh cs in allCloth) {
			GameObject kk = cs.gameObject;
			kk.layer = 0;
		}
			

		SkinnedMeshRenderer sm = currentItemCol.GetComponentInChildren<SkinnedMeshRenderer> ();
		sm.material = tmpMat;

	

		tmpMat.mainTexture = getTex;
		

	}
	// Use this for initialization
	void Update()
	{

		if (newtime > 0 & startDrip) {
			dripMeshRen.SetBlendShapeWeight (size, blendW * newtime/timer);
			print (100f -( blendW * newtime/timer));
		
			progessRB.Value = (100f -( blendW * newtime/timer));

			newtime =newtime - Time.deltaTime;


		}
		else if (tran & newtime<0){
			tran = false;
			kickOff ();
			startDrip = false;
			newtime = 1f;
		}

	}
}
