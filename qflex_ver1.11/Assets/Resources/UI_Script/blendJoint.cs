using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blendJoint : MonoBehaviour {

	public GameObject targetJoint;
	List<GameObject> tarJoint , sourJoint,holdVal;
	public float speed = 1.0f;
	float startTime;
	float interval = 0f;
	public bool startBJ =false ;
	Vector3 tarTran,tarRot,holdTran,holdRot;
	string[] joint  = new string[]{"Root_M","BackA_M","Chest_M","Scapula_L","Shoulder_L","ShoulderPart1_L","ShoulderPart2_L","Elbow_L","Wrist_L","Hip_L" , "Knee_L" ,"Ankle_L" ,"MiddleToe1_L","Scapula_R","Shoulder_R","ShoulderPart1_R","ShoulderPart2_R","Elbow_R","Wrist_R","Hip_R" , "Knee_R" ,"Ankle_R" ,"MiddleToe1_R"};
	// Use this for initialization
	public void intiBJ () {
		tarJoint = new List<GameObject> ();
		sourJoint = new List<GameObject> ();

		Transform[] tarTmp = targetJoint.GetComponentsInChildren<Transform>();
		Transform[] sourTmp = GetComponentsInChildren<Transform>();
		startTime = Time.time;
		for (int o =0; o<sourTmp.Length; o++) {
			for (int i = 0; i < joint.Length; i++) {
				if (sourTmp[o].name == joint[i]) {
					sourJoint.Add (sourTmp[o].gameObject);

					
				}
				if (tarTmp[o].name == joint[i]) {
					
					tarJoint.Add (tarTmp[o].gameObject);

				}
			}
		}


		holdVal = sourJoint;
		holdTran = transform.localPosition;
		holdRot = transform.localEulerAngles;

	}

	// Update is called once per frame
	void Update () {
		tarTran = targetJoint.transform.localPosition;
		tarRot = targetJoint.transform.localEulerAngles;
		if (startBJ) {
			interval = interval + Time.deltaTime * speed;
			//interval=1f;
			float tmpBTx = Mathf.Lerp (holdTran.x,tarTran.x, interval);
			float tmpBTy = Mathf.Lerp (holdTran.y, tarTran.y, interval);
			float tmpBTz = Mathf.Lerp (holdTran.z, tarTran.z, interval);
			float tmpBRx = Mathf.Lerp (WrapAngle (holdRot.x), WrapAngle (tarRot.x), interval);
			float tmpBRy = Mathf.Lerp (WrapAngle (holdRot.y), WrapAngle (tarRot.y), interval);
			float tmpBRz = Mathf.Lerp (WrapAngle (holdRot.z), WrapAngle (tarRot.z), interval);
			transform.localPosition= new Vector3 (tmpBTx,  tmpBTy, tmpBTz);
			transform.localEulerAngles = new Vector3 (tmpBRx, tmpBRy, tmpBRz);
			for (int m = 0; m < tarJoint.Count; m++) {
				float tmpRx = Mathf.Lerp (WrapAngle (holdVal [m].transform.localEulerAngles.x), WrapAngle (tarJoint [m].transform.localEulerAngles.x), interval);
				float tmpRy = Mathf.Lerp (WrapAngle (holdVal [m].transform.localEulerAngles.y), WrapAngle (tarJoint [m].transform.localEulerAngles.y), interval);
				float tmpRz = Mathf.Lerp (WrapAngle (holdVal [m].transform.localEulerAngles.z), WrapAngle (tarJoint [m].transform.localEulerAngles.z), interval);
				sourJoint [m].transform.localEulerAngles = new Vector3 (tmpRx, tmpRy, tmpRz);
				float tmpTx = Mathf.Lerp (holdVal [m].transform.localPosition.x, tarJoint [m].transform.localPosition.x, interval);
				float tmpTy = Mathf.Lerp (holdVal [m].transform.localPosition.y, tarJoint [m].transform.localPosition.y, interval);
				float tmpTz = Mathf.Lerp (holdVal [m].transform.localPosition.z, tarJoint [m].transform.localPosition.z, interval);
				sourJoint [m].transform.localPosition = new Vector3 (tmpTx, tmpTy, tmpTz);
			}
		}

	}
	private static float WrapAngle(float angle)
	{
		angle%=360f;


		 if(angle >180f)
		{
			return   angle - 360f ;
		}

		//if (angle < - 180) {
		//	return   angle+360;
		//}
		return angle;
	}
}
