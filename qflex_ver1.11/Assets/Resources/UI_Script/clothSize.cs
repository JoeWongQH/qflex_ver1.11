﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using uFlex;




public class clothSize: MonoBehaviour {

	public avatarDropM atD;

	public clothSet[] genderSet = new clothSet[2];

	public int currentClothBut=0;

	public Dropdown drop;

	public void setSize (int inSize)
	{
		bool sex = atD.gender [atD.drop.value - 1];

		atD.size = inSize;
		foreach (GameObject cs in atD.clothSet) {
			cs.SetActive (false);
		}
		if (sex) {
			atD.clothSet [atD.drop.value - 1] = genderSet [1].allSet [inSize]; 	
		} else {
		
			atD.clothSet [atD.drop.value - 1] = genderSet [0].allSet [inSize]; 	
		}

			atD.clothSet [atD.drop.value - 1].SetActive (true);


	}
	public void haveCloth(int inSize)
	{
		FlexParticles clothExist = FindObjectOfType<FlexParticles>();
		matrixParent mT = FindObjectOfType<matrixParent> ();
		if (clothExist) {
			//RuntimeAnimatorController timeToRead = atD.animatorCol.runtimeAnimatorController ;
		
			Animator aT = atD.currentItemCol.GetComponent<Animator> ();
			aT.enabled = false;
			RuntimeAnimatorController tmpRun = aT.runtimeAnimatorController;
			atD.wasteCol = atD.currentItemCol;
			AnimatorStateInfo aF = aT.GetCurrentAnimatorStateInfo (0);
			AnimatorClipInfo[] aC = aT.GetCurrentAnimatorClipInfo (0);
			atD.aStateName = aC[0].clip.name;
			atD.recordTime = aF.normalizedTime%1f;
			FlexSolver solver = FindObjectOfType<FlexSolver>();
			GameObject solverObj = solver.gameObject; 
			Destroy (solverObj);
			Instantiate (atD.FS);
			GameObject intermedCol = Instantiate (atD.menuItemCol[drop.value - 1]);

			atD.currentItemCol = intermedCol;
			atD.SetLayerRecursively (atD.currentItemCol, 8);
			Destroy (clothExist);
			Destroy (mT);
			GameObject newIdle = Instantiate (atD.menuItemIdle [drop.value - 1]);
			atD.currentItemIdle = newIdle;
			atD.SetLayerRecursively (atD.currentItemIdle , 8);
			SetLayerRecursively (atD.currentItemIdle, 8);
			blendJoint newBj = newIdle.AddComponent<blendJoint> ();
			newBj.targetJoint = atD.currentItemCol;
			newBj.speed =.4f;
			atD.bj = newBj;
			SkinnedMeshRenderer oldSk = atD.currentItemCol.GetComponentInChildren<SkinnedMeshRenderer> ();
			oldSk.sharedMaterial.mainTexture =null;
			Animator curAnim = atD.currentItemCol.GetComponent<Animator> ();

		 	curAnim.runtimeAnimatorController=atD.menuItemAnim[drop.value - 1];
		
			//atD.animatorCol = curAnim;
			curAnim.enabled = false;

			curAnim.updateMode = AnimatorUpdateMode.AnimatePhysics;


			MeshCollider[] allMC =FindObjectsOfType<MeshCollider>();
			foreach (MeshCollider MC in allMC) {
				Destroy (MC);
			}
			BoxCollider[] allBC =FindObjectsOfType<BoxCollider>();
			foreach (BoxCollider BC in allBC) {
				Destroy (BC);
			}
			atD.currentItemIdle.SetActive (true);
			//atD.timer = .8f;
			atD.tran = true;
			print("OK");
			setSize (inSize);


		}
		
	}
	void SetLayerRecursively(GameObject obj, int newLayer)
	{
		if (null == obj)
		{
			return;
		}

		obj.layer = newLayer;

		foreach (Transform child in obj.transform)
		{
			if (null == child)
			{
				continue;
			}
			SetLayerRecursively(child.gameObject, newLayer);
		}
	}
}
