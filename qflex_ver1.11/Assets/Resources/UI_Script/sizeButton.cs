﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sizeButton : MonoBehaviour {

	public itemSelect itS;

	public List<string> sizeList;

	public Text textSize; 

	int arrayCount ;

	public void searchClothObj()
	{	
		sizeList.Clear ();
		Object[] allClothObj;
		allClothObj = Resources.LoadAll ("Models", typeof(GameObject));
		string[] allClothName = new string[allClothObj.Length];
		for (int i = 0; i < allClothObj.Length; i++) {
			string[] tmpSt = allClothObj [i].name.Split ("_"[0]);
			allClothName [i] = tmpSt [0];

			print (allClothName[i]);
			if (allClothName [i] == itS.finClothN) {
				sizeList.Add (tmpSt [tmpSt.Length-1]);
			}

		}
		textSize.text = sizeList [0]; 
		arrayCount = sizeList.Count - 1;
	}


	public void sizeUp()
	{
		arrayCount ++ ;
		arrayCount = Mathf.Clamp (arrayCount, 0, (sizeList.Count - 1));
		textSize.text = sizeList [arrayCount]; 
		itS.size =  sizeList [arrayCount]; 

	}
	public void sizeDown()
	{
		arrayCount -- ;
		arrayCount = Mathf.Clamp (arrayCount, 0, (sizeList.Count - 1));
		textSize.text = sizeList [arrayCount]; 
		itS.size =  sizeList [arrayCount]; 
	}
}
