﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class camFocus : MonoBehaviour {

	public Camera cam;
	public Slider foSl;
	public GameObject camPiv;

	int camState = 0;

	public void switchCamera()
	{	
		camState++;
		camState = camState % 3;

		switch (camState){

		case 0:
			camPiv.transform.localPosition = new Vector3 (0f, 0.48f, 0f);
			cam.fieldOfView = 48f;
			break;
		case 1:
			camPiv.transform.localPosition = new Vector3 (-.02f, 0.75f, 0.0f);
			cam.fieldOfView = 25f;

			break;
		case 2:
			camPiv.transform.localPosition = new Vector3 (-.02f, 0.116f, 0.0f);
			cam.fieldOfView = 25f;

			break;
		}
		foSl.value = cam.fieldOfView / 120f;

	}
	void Start ()
	{
	
		changeFo ();
	
	}
	public void changeFo()
	{
		float focus = foSl.value;

		cam.fieldOfView = focus * 120f;
	
	}
}
