﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class getListInFo : MonoBehaviour {



	public string inPath;
	Vector3 dPos= new Vector3(-110f,-30f,0f) ;
	public GameObject spritePrefab;
   Object[] item ;
	public GameObject content;
	public getListInFo gLI;
	public itemSelect itS ;
	public sizeButton sB;
	public Material Mat;
	public Image defaultImage;
	public GameObject[] texSpriteContent;
	public string patternPath;
	public void loadSprite()

	{
		if (item == null) {
			item = Resources.LoadAll (inPath, typeof(Sprite));
			print (item);
			for (int i = 0; i < item.Length; i++) {
		
				GameObject tmp =	Instantiate (spritePrefab, content.transform);
				Image tmpImage = tmp.GetComponent<Image> ();
				tmpImage.sprite = item [i] as Sprite;
				Button but = tmp.GetComponent<Button> ();
				but.onClick.AddListener (itS.finCloth);
				but.onClick.AddListener (sB.searchClothObj);
				but.onClick.AddListener (gLI.loadtexture);

				//print (item[i]);
			}

		}
	}
	public void loadtexture()
	{
		if (texSpriteContent != null) {
			for (int  n = 0 ; n<texSpriteContent.Length; n++)
			Destroy (texSpriteContent[n]);
		}
		 string defaultName = gLI.defaultImage.sprite.name;
				print (defaultName);

		Object[]  texCloth= Resources.LoadAll (patternPath, typeof(Texture2D));
		texSpriteContent = new GameObject[texCloth.Length];
		for (int m = 0; m < texCloth.Length; m++) {
			string tmpName = texCloth [m].name.Split ("_" [0]) [0];
			if (tmpName==defaultName)
			{
				
			 texSpriteContent[m] =	Instantiate (spritePrefab, content.transform);
			Material texMat = new Material (Mat);
			texMat.mainTexture = texCloth [m] as Texture2D;
			Image tmpTex =  texSpriteContent[m].GetComponent<Image> ();
			tmpTex.material = texMat;
			Button but =  texSpriteContent[m].GetComponent<Button> ();
			but.onClick.AddListener (gLI.confirmMat);
			//print (texCloth [m]);
			}
		}
			
	}
	public void confirmMat()
	{
	

	
		Texture finTex =  EventSystem.current.currentSelectedGameObject.GetComponent<Image> ().material.mainTexture;
		print (finTex);


		itS.finTex = finTex; 
	}
}
