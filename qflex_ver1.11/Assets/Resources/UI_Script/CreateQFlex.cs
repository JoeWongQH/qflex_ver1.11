using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System;
using UnityEngine;
using uFlex;
using System.Linq;
using UnityEngine.UI;

namespace uFlex
{

public class CreateQFlex :  FlexProcessor {
	
	public avatarDropM atD;
	//public itemSelect itS;
		public getListInFo gLI_Top,gLI_Bot,gLI_Out,gLI_Ass;
	string newName = "Cloth";
	float mass = 1.0f;
	float stretchStiffness = 2.0f;
	float bendStiffness =1f;
	float tetherStiffness = 0.0f;
	float tetherGive = 0.0f;
	float weldThreshold = 0.0000f;
	Material clothMat ; 
	float maxStrain = 2.0f;
	int maxSplits = 4;
	public GameObject upperData, lowerData, outterData, assetsData;
	bool tearable = false;
	FlexInteractionType interactionType = FlexInteractionType.None;
	int group = -1;
	Flex.Phase phase = 0;
	Mesh inputMesh;
	Color color;
	bool softMesh = true;
		public matrixParent upM, loM, ouM;
	GameObject go;

	public void assMask(Texture2D inTex)
		{
			
			atD.getTex = inTex;
		
		
		}
		//public void pinJoint(string inJoint)
	//	{
			//atD.colAddJointName = inJoint.Split (";"[0]);
					
	//	}
		public void addPin(Mesh meshToPin)
		{
			
			GameObject pP = GameObject.Find (meshToPin.name);
			matrixParent mP = pP.AddComponent<matrixParent> ();
			mP.enabled=false;
				//mP.parent = new GameObject[jName.Length];
			//	for (int j = 0; j < jName.Length; j++) {
			//		mP.parent [j] = jName [j];
			//	}

		}

		public void pickMat(Material inMat)
		{

			clothMat = inMat;
		}
		public void getStiff(float inStiff)
		{
			
			stretchStiffness = inStiff;
		}

		public void collectDressData()
		{
			
		
		
		}
		public void collectCloth()
		{

			itemSelect upperResult = upperData.GetComponent<itemSelect> ();
			object[] tmpUpCloth =  Resources.LoadAll ("Models", typeof(GameObject));
			if (upperResult.size!=null) {
				Mesh uMesh=null;
				string combName = (upperResult.finClothN + "_" + upperResult.size);
				print (combName);
			

				for (int i = 0; i < tmpUpCloth.Length; i++) {
					GameObject tmpGo = tmpUpCloth [i] as GameObject;
					if (tmpGo.name == combName) {
						uMesh = tmpGo.GetComponentInChildren<MeshFilter> ().sharedMesh;
						print (uMesh);
					}

				}

				GameObject upRe = GenCloth (uMesh,upperResult.finTex);
				 upM = upRe.AddComponent<matrixParent> ();
				upM.enabled = false;
			}
			itemSelect lowerResult = lowerData.GetComponent<itemSelect> ();
			if (lowerResult.finSize!=null) {
				Mesh lMesh = lowerResult.finSize.GetComponentInChildren<MeshFilter> ().sharedMesh;
				Texture lMat = lowerResult.finTex;
				GameObject loRe = GenCloth (lMesh, lMat);
				 loM = loRe.AddComponent<matrixParent> ();
				loM.enabled = false;
			}
			itemSelect outterResult = outterData.GetComponent<itemSelect> ();
			if (outterResult.finSize!=null) {
				Mesh oMesh = outterResult.finSize.GetComponentInChildren<MeshFilter> ().sharedMesh;
				Texture oMat = outterResult.finTex;
				GameObject ouRe = GenCloth (oMesh, oMat);
				 ouM = ouRe.AddComponent<matrixParent> ();
				ouM.enabled = false;
			}

		}




		public GameObject GenCloth(Mesh inMesh ,Texture inMat)
	{
		inputMesh = inMesh;
		go = new GameObject();
		go.name = inputMesh.name;
		GenerateFromMesh(go);
		FlexParticlesRenderer partRend = go.AddComponent<FlexParticlesRenderer>();
		partRend.enabled = false;
		
		FlexSprings cSpring = go.GetComponent<FlexSprings> ();
		cSpring.m_overrideStiffness = true;
			cSpring.m_newStiffness = stretchStiffness;

			Renderer MS;
			Material Mat,newMat,cloneMat;
			MS = go.GetComponent<Renderer> ();
			Mat = MS.sharedMaterial;
	
			cloneMat = new Material(Shader.Find("Example/Normal Extrusion"));
			cloneMat.SetColor ("_MainTint",new Color(.2f,.2f,.2f,1f));
			cloneMat.mainTexture = inMat;
			MS.sharedMaterial = cloneMat;
			atD.currentCloth = go;
			go.layer = 8;
			return go;
		 	
	}

		public void setBlend(float inBlend)
		{
			bendStiffness = inBlend;
		
		}


private  void GenerateFromMesh(GameObject go)
	{
		color = new Color (UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value, 1.0f);
		Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
		Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

		Vector3[] vertices = inputMesh.vertices;
		int vertexCount = inputMesh.vertexCount;

		int[] triangles = inputMesh.triangles;
		int triIndicesCount = triangles.Length;
		int trianglesCount = triIndicesCount / 3;

		IntPtr flexAssetPtr = IntPtr.Zero;
		int[] uniqueVerticesIds = new int[vertexCount];
		int[] origToUniqueVertMapping = new int[vertexCount];
		int uniqueVerticesCount = FlexExt.flexExtCreateWeldedMeshIndices(vertices, vertexCount,  uniqueVerticesIds,  origToUniqueVertMapping, weldThreshold);
		Vector3[] uniqueVertices = new Vector3[uniqueVerticesCount];
		Vector4[] uniqueVerticesWithInvMass = new Vector4[uniqueVerticesCount];
		for (int i = 0; i < uniqueVerticesCount; i++)
		{
			uniqueVertices[i] = vertices[uniqueVerticesIds[i]];

			uniqueVerticesWithInvMass[i] = vertices[uniqueVerticesIds[i]];
			uniqueVerticesWithInvMass[i].w = 1.0f / this.mass;

			min = FlexUtils.Min(min, uniqueVertices[i]);
			max = FlexUtils.Max(max, uniqueVertices[i]);
		}
		int[] uniqueTriangles = new int[trianglesCount*3];
		for (int i = 0; i < trianglesCount * 3; i++)
		{
			uniqueTriangles[i] = origToUniqueVertMapping[triangles[i]];
		}

		flexAssetPtr = FlexExt.flexExtCreateClothFromMesh(uniqueVerticesWithInvMass, uniqueVerticesCount, uniqueTriangles, trianglesCount, stretchStiffness, bendStiffness, tetherStiffness, tetherGive, 0);
		if (flexAssetPtr != IntPtr.Zero)
		{      
			//Flex Asset Marshalling
			FlexExt.FlexExtAsset flexAsset = (FlexExt.FlexExtAsset)Marshal.PtrToStructure(flexAssetPtr, typeof(FlexExt.FlexExtAsset));
			Vector4[] particles = FlexUtils.MarshallArrayOfStructures<Vector4>(flexAsset.mParticles, flexAsset.mNumParticles);

			int[] springIndices = FlexUtils.MarshallArrayOfStructures<int>(flexAsset.mSpringIndices, flexAsset.mNumSprings * 2);
			float[] springRestLengths = FlexUtils.MarshallArrayOfStructures<float>(flexAsset.mSpringRestLengths, flexAsset.mNumSprings);
			float[] springCoefficients = FlexUtils.MarshallArrayOfStructures<float>(flexAsset.mSpringCoefficients, flexAsset.mNumSprings);

			int[] shapeIndices = FlexUtils.MarshallArrayOfStructures<int>(flexAsset.mShapeIndices, flexAsset.mNumShapeIndices);
			int[] shapeOffsets = FlexUtils.MarshallArrayOfStructures<int>(flexAsset.mShapeOffsets, flexAsset.mNumShapes);
			Vector3[] shapeCenters = FlexUtils.MarshallArrayOfStructures<Vector3>(flexAsset.mShapeCenters, flexAsset.mNumShapes);
			float[] shapeCoefficients = FlexUtils.MarshallArrayOfStructures<float>(flexAsset.mShapeCoefficients, flexAsset.mNumShapes);


			if (flexAsset.mNumParticles > 0)
			{
				FlexParticles part = go.AddComponent<FlexParticles>();
				part.m_particlesCount = flexAsset.mNumParticles;
				part.m_maxParticlesCount = flexAsset.mMaxParticles;
				part.m_particles = new Particle[flexAsset.mMaxParticles];
				part.m_restParticles = new Particle[flexAsset.mMaxParticles];
				part.m_smoothedParticles = new Particle[flexAsset.mMaxParticles];
				part.m_colours = new Color[flexAsset.mMaxParticles];
				part.m_velocities = new Vector3[flexAsset.mMaxParticles];
				part.m_densities = new float[flexAsset.mMaxParticles];
				part.m_phases = new int[flexAsset.mMaxParticles];
				part.m_particlesActivity = new bool[flexAsset.mMaxParticles];


				part.m_colour = color;
				part.m_interactionType = interactionType;
				part.m_collisionGroup = group;
				part.m_bounds.SetMinMax(min, max);

				for (int i = 0; i < flexAsset.mNumParticles; i++)
				{
					part.m_particles[i].pos = particles[i];
					part.m_particles[i].invMass = particles[i].w;
					part.m_restParticles[i] = part.m_particles[i];
					part.m_smoothedParticles[i] = part.m_particles[i];
					part.m_colours[i] = color;
					part.m_particlesActivity[i] = true;


					part.m_phases[i] = (int)phase;



					//if (spacingRandomness != 0)
					//{
					//    part.m_particles[i].pos  += UnityEngine.Random.insideUnitSphere * spacingRandomness;
					//}
				}

			}


			if (flexAsset.mNumSprings > 0)
			{
				FlexSprings springs = go.AddComponent<FlexSprings>();
				springs.m_springsCount = flexAsset.mNumSprings;
				springs.m_springIndices = springIndices;
				springs.m_springRestLengths = springRestLengths;
				springs.m_springCoefficients = springCoefficients;


			}

			if(flexAsset.mNumTriangles > 0)
			{
				FlexTriangles tris = go.AddComponent<FlexTriangles>();
				tris.m_trianglesCount = trianglesCount;
				tris.m_triangleIndices = uniqueTriangles; 

			}


			FlexShapeMatching shapes = null;
			if (flexAsset.mNumShapes > 0)
			{
				shapes = go.AddComponent<FlexShapeMatching>();
				shapes.m_shapesCount = flexAsset.mNumShapes;
				shapes.m_shapeIndicesCount = flexAsset.mNumShapeIndices;
				shapes.m_shapeIndices = shapeIndices;
				shapes.m_shapeOffsets = shapeOffsets;
				shapes.m_shapeCenters = shapeCenters;
				shapes.m_shapeCoefficients = shapeCoefficients;
				shapes.m_shapeTranslations = new Vector3[flexAsset.mNumShapes];
				shapes.m_shapeRotations = new Quaternion[flexAsset.mNumShapes];
				shapes.m_shapeRestPositions = new Vector3[flexAsset.mNumShapeIndices];

				int shapeStart = 0;
				int shapeIndex = 0;
				int shapeIndexOffset = 0;
				for (int s = 0; s < shapes.m_shapesCount; s++)
				{
					shapes.m_shapeTranslations[s] = new Vector3();
					shapes.m_shapeRotations[s] = Quaternion.identity;

					//    int indexOffset = shapeIndexOffset;

					shapeIndex++;

					int shapeEnd = shapes.m_shapeOffsets[s];

					for (int i = shapeStart; i < shapeEnd; ++i)
					{
						int p = shapes.m_shapeIndices[i];

						// remap indices and create local space positions for each shape
						Vector3 pos = particles[p];
						shapes.m_shapeRestPositions[shapeIndexOffset] = pos - shapes.m_shapeCenters[s];

						//   m_shapeIndices[shapeIndexOffset] = shapes.m_shapeIndices[i] + particles.m_particlesIndex;

						shapeIndexOffset++;
					}

					shapeStart = shapeEnd;



				}
			}

			if (flexAsset.mInflatable)
			{
				FlexInflatable infla = go.AddComponent<FlexInflatable>();
				infla.m_pressure = flexAsset.mInflatablePressure;
				infla.m_stiffness = flexAsset.mInflatableStiffness;
				infla.m_restVolume = flexAsset.mInflatableVolume;

			}
			if (softMesh)
			{

				Renderer rend = null;


				if (tearable)
				{
					FlexTearableMesh tearableMesh = go.AddComponent<FlexTearableMesh>();
					tearableMesh.m_stretchKs = stretchStiffness;
					tearableMesh.m_bendKs = bendStiffness;
					tearableMesh.m_maxSplits = maxSplits;
					tearableMesh.m_maxStrain = maxStrain;
				}
				else
				{
					go.AddComponent<FlexClothMesh>();
				}

				MeshFilter meshFilter = go.AddComponent<MeshFilter>();
				meshFilter.sharedMesh = this.inputMesh;
				rend = go.AddComponent<MeshRenderer>();



				Material mat = new Material(Shader.Find("Diffuse"));
				mat.name = this.newName + "Mat";
				mat.color = color;
				rend.material = mat;

			}

		}

	}

	
}
}