using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using uFlex;
public class LoadChara : MonoBehaviour {

	GameObject outModelQHm,outColModel;
	float timer =1.5f;
	float blendW=100;
	bool startDrip = false;
	bool tran = true;
	SkinnedMeshRenderer dripMeshRen;
	GameObject	skinObj,colObj;
	RuntimeAnimatorController Anim;
	float newtime;
	GameObject[] jName ;
	blendJoint bj;
	deformCollider dC;
	MeshCollider mC;
	RuntimeAnimatorController humanMot;
	Animator animatorCol;

	string[] colAddJointName= new string[]{"Scapula_L","Scapula_R","Wrist_L","Wrist_R","Ankle_L","Ankle_R"};
	public void load(string inPath)
	{
				
		object[] QHmodel;
		GameObject tmpModel;
		GameObject dripQH; 
		getChildWithName gJoint = new getChildWithName ();
		RuntimeAnimatorController anim;
		BoxCollider[] bCol = new BoxCollider[2];

		QHmodel =  Resources.LoadAll("QHuman",typeof(GameObject)) ;
		if (outModelQHm == null) {
			for (int i = 0; i < QHmodel.Length; i++) {


				tmpModel = QHmodel [i] as GameObject;  
				if (tmpModel.name == inPath) {
					dripQH = QHmodel [i] as GameObject; 
					outModelQHm = Instantiate (dripQH);	
					SetLayerRecursively (outModelQHm, 8);
				}

		
			}
		}
		anim = Anim;
		animatorCol=outModelQHm .GetComponent<Animator> ();
		animatorCol.enabled=false;
		animatorCol.runtimeAnimatorController = anim;
		animatorCol.updateMode = AnimatorUpdateMode.AnimatePhysics;
		jName = gJoint.getChildWName (outModelQHm, colAddJointName);
		foreach (GameObject tmpObj in jName)
		{
			BoxCollider BC = tmpObj.AddComponent<BoxCollider> ();
			BC.size = new Vector3 (.05f, .05f, .05f);
			if (tmpObj.name == "Scapula_L") {
				BC.center = new Vector3 (.05f, -0.04f, -0.02f);
			}
			if (tmpObj.name == "Scapula_R") {
				BC.center = new Vector3 (-0.05f, 0.04f, 0.02f);
			}
		}
	}

	public void deModel()
	{

		Destroy(outModelQHm);
	}

	private GameObject getQHmCollider(GameObject inColObj)
	{
		GameObject outCollQHmM; 


		outCollQHmM =Instantiate (inColObj);
	
		return outCollQHmM;

	}
	public void setBlendjoint(GameObject inCol)
	{
		colObj =  Instantiate (inCol);
		bj=colObj.AddComponent<blendJoint> ();
		bj.targetJoint = outModelQHm;
	}
	public void setAnim(RuntimeAnimatorController inAnim)
	{
		Anim = inAnim;
	}
	public void drip()
	{
		

		dripMeshRen = colObj.GetComponentInChildren<SkinnedMeshRenderer> ();
		GameObject colSkinObj  =dripMeshRen.gameObject;
	 	dripMeshRen.SetBlendShapeWeight (0,100);
		dC =colSkinObj.AddComponent<deformCollider>();
		mC=colSkinObj.AddComponent<MeshCollider>();
		MeshFilter tmpMFilter = colSkinObj .AddComponent<MeshFilter> ();
		animatorCol.enabled=true;
	
	}
	void SetLayerRecursively(GameObject obj, int newLayer)
	{
		if (null == obj)
		{
			return;
		}

		obj.layer = newLayer;

		foreach (Transform child in obj.transform)
		{
			if (null == child)
			{
				continue;
			}
			SetLayerRecursively(child.gameObject, newLayer);
		}
	}

	public void kickOn()
	{	
		FlexSolver solver = FindObjectOfType<FlexSolver>();
		solver.deform_collider = true;
		solver.enabled = true;
		startDrip = true;
		bj.intiBJ();
		bj.startBJ = true;

		newtime = timer;

	}
	private void kickOff()
	{	
		

		FlexSolver solver = FindObjectOfType<FlexSolver>();
	//FlexColliders fCol = FindObjectOfType<FlexColliders> ();
		matrixParent[] mP = FindObjectsOfType<matrixParent> ();
		bj.enabled = false;
		foreach (matrixParent mPTmp in mP) {
			mPTmp.parent = new GameObject[jName.Length];

			for (int m = 0; m < jName.Length; m++) {
				mPTmp.parent [m] = jName [m];
			
			}
			mPTmp.getInstancePin();
			mPTmp.active = true;
		}
	
		solver.enabled = false;
		solver.deform_collider = false;
		dC.enabled = false;
		mC.enabled = false;
		colObj.SetActive(false);
		SetLayerRecursively (outModelQHm, 1);
		outColModel = outModelQHm;
		MeshFilter[]	mm = outColModel.GetComponentsInChildren<MeshFilter> ();

		GameObject[] allColObj =new GameObject[mm.Length];
		for (int i = 0; i < mm.Length; i++) {

			allColObj [i] = mm [i].gameObject;
			allColObj [i].AddComponent (typeof(MeshCollider));
			allColObj [i].GetComponent<MeshRenderer>().enabled = false;
			Debug.Log (allColObj[i]);
		}


		solver.m_colliders.ProcessColliders(solver.m_solverPtr,Flex.Memory.eFlexMemoryHost);


		solver.enabled = true;
	}


	// Use this for initialization
	void Update()
	{
		
		if (newtime > 0 & startDrip) {
			dripMeshRen.SetBlendShapeWeight (0, blendW * newtime/timer);
			newtime =newtime - Time.deltaTime;

			print (newtime);
		}
		else if (tran & newtime<0){
			tran = false;
			kickOff ();

		}

	}
}
