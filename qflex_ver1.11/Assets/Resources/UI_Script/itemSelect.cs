﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


[System.Serializable]
public class clothSet2
{

	public GameObject[] allSet2; 

}
[System.Serializable]
public class matteSet
{
public Texture2D[] allmatte;
}

public class itemSelect : MonoBehaviour{


	// Use this for initialization

	public Image[] selectImage2,holdImage;
	public Sprite[] clothSetSprite;
	public clothSet2[] clothItem =new clothSet2[3];
	public Material[] allCMat;
	public Texture finTex;
	public string[] pinJoint;
	public string[] finJoint;
	public GameObject sizeUI;
	int[] holdDsp,oldTmp;

	public matteSet[] matteItem = new matteSet[3];
	public avatarDropM aDM;
	int selCount = 0;
	public Image conP;
	public Animator[] buttonAR2; 
	public string finClothN,size;
	public GameObject finSize;  
	public List<string> sizeList;
	 void Start()
	{
		holdDsp  = new int[clothSetSprite.Length];
		oldTmp = new int[clothSetSprite.Length];
		holdImage = new Image[ selectImage2.Length];
		for (int i = 0; i < clothSetSprite.Length; i++) {
		
			holdDsp[i] = i;
			oldTmp[i] = i;
		}
		for (int i = 0; i < selectImage2.Length; i++) {
			
			holdImage[i] =selectImage2[i] ;
		}

	}
	public void setSize(int inSize)
	{	
		selCount = holdDsp [1];
		clothSet2 CS = clothItem [inSize];
		finSize = CS.allSet2[selCount] ;
		//finMat = allCMat [selCount];
		finJoint = pinJoint [selCount].Split (";" [0]);
		matteSet tmpMatte = matteItem [inSize];
		aDM.getTex =tmpMatte.allmatte[holdDsp [1]] ;
	}

	public void rightClick()
	{
		selectImage2 [1].gameObject.transform.DetachChildren ();
		int holdlast = holdDsp [clothSetSprite.Length-1];
		for (int i =1; i <clothSetSprite.Length; i++) {
			 

			holdDsp [i] = oldTmp [i - 1];

		}
		holdDsp [0] = holdlast;
		for (int i =0; i <clothSetSprite.Length; i++) {


			oldTmp [i] = holdDsp [i];

		}
		Image holdSetlast = selectImage2 [selectImage2.Length-1];
		for (int i =1; i <selectImage2.Length; i++) {


			selectImage2[i] = holdImage[i - 1];

		}
		selectImage2  [0] = holdSetlast;
		for (int i =0; i <selectImage2.Length; i++) {



			holdImage[i] =selectImage2[i] ;

		}

		for (int i =0; i <selectImage2.Length; i++) {



			selectImage2[i].sprite= clothSetSprite[holdDsp [i]] ;

		}
		sizeUI.transform.SetParent (selectImage2 [1].gameObject.transform);
	
		sizeUI.transform.localPosition =new Vector3 (-3, 0, 0);
		sizeUI.transform.localScale =new  Vector3 (1, 1, 1);
		//print (holdDsp [0]+"" + holdDsp [1]+"" + holdDsp [2]+"" +holdDsp [3]+ "" +holdDsp [4]);
	}

	public void leftClick()
	{
		int holdFir = holdDsp [0];
		for (int i =0; i <clothSetSprite.Length-1; i++) {


			holdDsp [i] = oldTmp [i +1];

		}
		holdDsp [clothSetSprite.Length-1] = holdFir;
		for (int i =0; i <clothSetSprite.Length; i++) {


			oldTmp [i] = holdDsp [i];

		}
		Image holdSetlast = selectImage2 [0];
		for (int i =0; i <selectImage2.Length-1; i++) {


			selectImage2[i] = holdImage[i + 1];

		}
		selectImage2  [2] = holdSetlast;
		for (int i =0; i <selectImage2.Length; i++) {



			holdImage[i] =selectImage2[i] ;

		}

		for (int i =0; i <selectImage2.Length; i++) {



			selectImage2[i].sprite= clothSetSprite[holdDsp [i]] ;


		}
		sizeUI.transform.SetParent (selectImage2 [1].gameObject.transform);

		sizeUI.transform.localPosition =new Vector3 (-3, -0, 0);
		sizeUI.transform.localScale =new  Vector3 (1, 1, 1);
	}

	public void cToLeft()
	{
		for (int i = 0; i < 3; i++) {

			switch (buttonAR2[i].GetInteger ("state")) {


			case 2: 
				buttonAR2[i].SetInteger ("state", 1);
			//buttonAR.ResetTrigger ("center_right");
				buttonAR2[i].SetTrigger ("normal");
				break;
			case 1:


				buttonAR2[i].SetTrigger ("center_left");
				buttonAR2[i].SetInteger ("state", 0);
		
				break;

			case 0:
			//buttonAR.ResetTrigger ("center_left");
				buttonAR2[i].SetTrigger ("left_go");
				buttonAR2[i].SetInteger ("state", 2);
			//buttonAR.SetTrigger ("normal_right");

				break;
			}

		}

	}
	public void cToRight()
	{	
		for (int j = 0; j < 3; j++) {
			switch (buttonAR2[j].GetInteger ("state")) {

			case 2:
			//buttonAR.ResetTrigger ("center_right");
				buttonAR2[j].SetInteger ("state", 0);
				buttonAR2[j].SetTrigger ("right_go");

				break;
		
			case 1:
				buttonAR2[j].SetInteger ("state", 2);
				buttonAR2[j].SetTrigger ("center_right");
				break;
	
			case 0:
				buttonAR2[j].SetInteger ("state", 1);
				buttonAR2[j].SetTrigger ("normal");
				break;
			}
		}
	}
	public void confirmPic()
	{
	
		conP.enabled = true;
		conP.sprite = clothSetSprite [holdDsp [1]];
	
	}
	public void testPrint()
	{
		print("OK");

	}

	public void finCloth()
	{
	
		GameObject curObj =EventSystem.current.currentSelectedGameObject ;
		//print (curObj.GetComponent<Image> ().sprite.name);
		Image curIm = curObj.GetComponent<Image> ();
		string clothName = curIm.sprite.name;
		conP.sprite = curIm.sprite;
		//print (clothName);
		finClothN = clothName;
	}
	public void searchClothObj()
	{	
		sizeList.Clear ();
		Object[] allClothObj;
		allClothObj = Resources.LoadAll ("Models", typeof(GameObject));
		string[] allClothName = new string[allClothObj.Length];
		for (int i = 0; i < allClothObj.Length; i++) {
			string[] tmpSt = allClothObj [i].name.Split ("_"[0]);
			allClothName [i] = tmpSt [0];

				print (allClothName[i]);
			if (allClothName [i] == finClothN) {
				sizeList.Add (tmpSt [tmpSt.Length-1]);
			}
			
		}
	
	}

}
