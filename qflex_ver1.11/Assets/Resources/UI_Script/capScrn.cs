﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class capScrn : MonoBehaviour {

	Texture2D scrCapT;
	Sprite scrCapSp;
	public Image inImage;

	IEnumerator cc;
	// Use this for initialization
	public void cap()
	{
		
		 Application.CaptureScreenshot ("Assets/Resources/ScreenShot/screen.png");


	}
	public void show ()
	{
		AssetDatabase.Refresh();
		scrCapT = Resources.Load<Texture2D> ("ScreenShot/screen") ;
		scrCapSp = Sprite.Create (scrCapT,new Rect(0f,0f,512f,512f),new Vector2(0f,0f));
		print (scrCapSp);
		inImage.enabled = true;
		inImage.sprite = scrCapSp;
	}
}
